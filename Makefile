

SHELL := /bin/bash

save:
	@echo "Pushing to gitlab..."
	git add -A
	@read -p "Enter commit message: " MSG; \
	git commit -a -m "$$MSG"
	git push origin master


## Push to gitlab (skipping continuous integration)
save-noci:
	@echo "Pushing to gitlab (no ci)..."
	@git add -A
	@read -p "Enter commit message: " MSG; \
	git commit -a -m "$$MSG [skip ci]"
	@git push origin $(BRANCH)
	
